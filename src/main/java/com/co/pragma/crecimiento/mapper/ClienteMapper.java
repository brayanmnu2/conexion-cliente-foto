package com.co.pragma.crecimiento.mapper;

import java.util.List;
import org.mapstruct.Mapper;
import com.co.pragma.crecimiento.dto.ClienteDto;
import com.co.pragma.crecimiento.dto.InformacionClienteDto;

@Mapper(componentModel = "spring")
public interface ClienteMapper {
	
	ClienteDto informacionClienteDtoToClienteDto(InformacionClienteDto informacion);

	InformacionClienteDto clientDtoToInformacionClienteDto(ClienteDto clienteDto);

	List<ClienteDto> listInformacionClienteDtoToListClienteDto(List<InformacionClienteDto> listClienteDto);

	List<InformacionClienteDto> listClienteDtoToListInformacionClienteDto(List<ClienteDto> listClienteEntity);
}
