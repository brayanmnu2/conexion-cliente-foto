package com.co.pragma.crecimiento.util;

import org.springframework.http.ResponseEntity;

import com.co.pragma.crecimiento.dto.ClienteDto;
import com.co.pragma.crecimiento.dto.ClienteFotoDto;
import com.co.pragma.crecimiento.dto.InformacionClienteDto;
import com.co.pragma.crecimiento.dto.TipoIdentificacionDto;

public class DatosTest {

	public static final Long UNO_LONG = 1L;
	public static final Long DOS_LONG = 2L;
	public static final Long TRES_LONG = 3L;
	public static final Long CUATRO_LONG = 4L;

	public static final String DNI_DESC = "DNI";

	public static final TipoIdentificacionDto TIPO_IDENTIFICACION_DTO_DNI = TipoIdentificacionDto.builder().id(UNO_LONG)
			.descripcion(DNI_DESC).build();
	public static final ResponseEntity<TipoIdentificacionDto> TIPO_IDENTIFICACION_DTO_RESPONSE_DNI_OK = ResponseEntity
			.ok(TIPO_IDENTIFICACION_DTO_DNI);

	public static final ClienteDto CLIENTE_DTO_SIN_ID_001 = ClienteDto.builder().nombres("Brayan Michel")
			.apellidos("Neyra Uriarte").tipoIdentificacion(TIPO_IDENTIFICACION_DTO_DNI).nroIdentificacion("75153630")
			.edad(24).ciudadNacimiento("Trujillo").build();
	
	public static final ClienteDto CLIENTE_DTO_MODIFICADO_001 = ClienteDto.builder().id(UNO_LONG).nombres("Brayitan Michel")
			.apellidos("Neyra Uriarte").tipoIdentificacion(TIPO_IDENTIFICACION_DTO_DNI).nroIdentificacion("75153630")
			.edad(24).ciudadNacimiento("Trujillo").build();

	public static final ClienteDto CLIENTE_DTO_001 = ClienteDto.builder().id(UNO_LONG).nombres("Brayan Michel")
			.apellidos("Neyra Uriarte").tipoIdentificacion(TIPO_IDENTIFICACION_DTO_DNI).nroIdentificacion("75153630")
			.edad(24).ciudadNacimiento("Trujillo").build();

	public static final ResponseEntity<ClienteDto> CLIENTE_DTO_RESPONSE_001 = ResponseEntity.ok(CLIENTE_DTO_001);
	public static final ResponseEntity<ClienteDto> CLIENTE_DTO_RESPONSE_MODIFICADO_001 = ResponseEntity.ok(CLIENTE_DTO_MODIFICADO_001);

	public static final ClienteFotoDto FOTO_DTO_001 = ClienteFotoDto.builder().id(UNO_LONG).img("imagen 1").build();
	public static final ResponseEntity<ClienteFotoDto> FOTO_DTO_RESPONSE_001 = ResponseEntity.ok(FOTO_DTO_001);

	public static final ClienteFotoDto FOTO_DTO_MODIFICADO_001 = ClienteFotoDto.builder().id(UNO_LONG).img("imagen 12345").build();
	public static final ResponseEntity<ClienteFotoDto> FOTO_DTO_MODIFICADO_RESPONSE_001 = ResponseEntity.ok(FOTO_DTO_MODIFICADO_001);
	
	public static final InformacionClienteDto INFORMACION_CLIENTE_DTO_001 = InformacionClienteDto.builder()
			.nombres("Brayan Michel").apellidos("Neyra Uriarte").idTipoIdentificacion(UNO_LONG)
			.nroIdentificacion("75153630").edad(24).ciudadNacimiento("Trujillo").img("imagen 1").build();
	
	public static final InformacionClienteDto INFORMACION_CLIENTE_RETURN_DTO_001 = InformacionClienteDto.builder()
			.id(UNO_LONG).nombres("Brayan Michel").apellidos("Neyra Uriarte").idTipoIdentificacion(UNO_LONG)
			.nroIdentificacion("75153630").edad(24).ciudadNacimiento("Trujillo").img("imagen 1").build();
	
	public static final InformacionClienteDto INFORMACION_CLIENTE_NO_VALID_DTO_001 = InformacionClienteDto.builder()
			.apellidos("Neyra Uriarte").idTipoIdentificacion(UNO_LONG)
			.nroIdentificacion("75153630").edad(24).ciudadNacimiento("Trujillo").img("imagen 1").build();

	public static final InformacionClienteDto INFORMACION_CLIENTE_DTO_MODIFICADO_001 = InformacionClienteDto.builder()
			.id(UNO_LONG).nombres("Brayitan Michel").apellidos("Neyra Uriarte").idTipoIdentificacion(UNO_LONG)
			.nroIdentificacion("75153630").edad(24).ciudadNacimiento("Trujillo").img("imagen 12345").build();
	
	public static final TipoIdentificacionDto TIPO_IDENTIFICACION_DTO_FALLBACK = TipoIdentificacionDto.builder().id(1L)
			.descripcion(null).build();

	public static final ClienteDto CLIENTE_DTO_SIN_ID_FALLBACK_001 = ClienteDto.builder().nombres("Brayan Michel")
			.apellidos("Neyra Uriarte").tipoIdentificacion(TIPO_IDENTIFICACION_DTO_FALLBACK)
			.nroIdentificacion("75153630").edad(24).ciudadNacimiento("Trujillo").build();

	public static final ResponseEntity<TipoIdentificacionDto> TIPO_IDENTIFICACION_DTO_RESPONSE_DNI_FALLBACK = ResponseEntity
			.ok(TIPO_IDENTIFICACION_DTO_FALLBACK);

	public static final ClienteDto CLIENTE_DTO_FALLBACK_001 = ClienteDto.builder().apellidos("").nombres("").edad(0)
			.nroIdentificacion("").ciudadNacimiento("").build();

	public static final ResponseEntity<ClienteDto> CLIENTE_DTO_RESPONSE_FALLBACK_001 = ResponseEntity
			.ok(CLIENTE_DTO_FALLBACK_001);

	public static final ClienteFotoDto FOTO_DTO_FALLBACK_001 = ClienteFotoDto.builder().id(null).img("").build();
	public static final ResponseEntity<ClienteFotoDto> FOTO_DTO_RESPONSE_FALLBACK_001 = ResponseEntity.ok(FOTO_DTO_FALLBACK_001);

}
