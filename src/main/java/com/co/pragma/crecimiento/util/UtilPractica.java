package com.co.pragma.crecimiento.util;

import java.io.IOException;
import java.util.Base64;
import org.springframework.web.multipart.MultipartFile;

public class UtilPractica {

	public String convertMultipartToBase64( MultipartFile file) {
		StringBuilder imagenString = new StringBuilder();
		imagenString.append("data:image/png;base64,");
		try {
			imagenString.append(Base64.getEncoder().encodeToString(file.getBytes()));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return imagenString.toString();
	}
	
}
