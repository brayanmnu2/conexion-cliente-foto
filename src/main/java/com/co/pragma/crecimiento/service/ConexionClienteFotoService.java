package com.co.pragma.crecimiento.service;


import com.co.pragma.crecimiento.dto.InformacionClienteDto;

public interface ConexionClienteFotoService {

	public InformacionClienteDto registrar(InformacionClienteDto informacion);
	
	public InformacionClienteDto actualizar(InformacionClienteDto informacion);
	
	public InformacionClienteDto mostrar(Long idCliente);
	
}
