package com.co.pragma.crecimiento.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import com.co.pragma.crecimiento.dto.ClienteDto;
import com.co.pragma.crecimiento.dto.ClienteFotoDto;
import com.co.pragma.crecimiento.dto.InformacionClienteDto;
import com.co.pragma.crecimiento.dto.TipoIdentificacionDto;
import com.co.pragma.crecimiento.mapper.ClienteMapper;
import com.co.pragma.crecimiento.proxy.ClienteServiceCl;
import com.co.pragma.crecimiento.proxy.FotoServiceCl;
import com.co.pragma.crecimiento.service.ConexionClienteFotoService;

@Service
public class ConexionClienteFotoServiceImpl implements ConexionClienteFotoService {

	@Autowired
	private ClienteServiceCl clienteService;

	@Autowired
	private FotoServiceCl fotoService;
	
	@Autowired
	private ClienteMapper clienteMapper;
	
	@Override
	public InformacionClienteDto registrar(InformacionClienteDto informacion) {
		//Llenar datos para registrar cliente
		ClienteDto clienteDto = clienteMapper.informacionClienteDtoToClienteDto(informacion);
		ResponseEntity<TipoIdentificacionDto> identificacionDto = clienteService.verDetalleIdentificacion(informacion.getIdTipoIdentificacion());
		if(null!=identificacionDto) {
			clienteDto.setTipoIdentificacion(identificacionDto.getBody());
		}
		
		//Registrar cliente
		ResponseEntity<ClienteDto> clienteReturn = clienteService.registrarCliente(clienteDto);
		
		InformacionClienteDto infomacionReturn =null;
		if(null!=clienteReturn) {
			infomacionReturn = clienteMapper.clientDtoToInformacionClienteDto(clienteReturn.getBody());	
			if(null!=clienteReturn.getBody().getTipoIdentificacion()) {
				infomacionReturn.setIdTipoIdentificacion(clienteReturn.getBody().getTipoIdentificacion().getId());	
			}
		}
		//Llenar datos para registrar foto
		ClienteFotoDto fotoDto =   null;
		if(null!=informacion && null!=clienteReturn && null!=clienteReturn.getBody()) {
			fotoDto = ClienteFotoDto.builder()
					.id(clienteReturn.getBody().getId())
					.img(informacion.getImg())
					.build();	
		}
		//Registrar foto
		ResponseEntity<ClienteFotoDto> fotoReturn = fotoService.registrarFoto(fotoDto);
		if(null!=fotoReturn && null!=fotoReturn.getBody() && null!=infomacionReturn) {
			infomacionReturn.setImg(fotoReturn.getBody().getImg());
		}
		
		return infomacionReturn;
	}

	@Override
	public InformacionClienteDto actualizar(InformacionClienteDto informacion) {
		// TODO Auto-generated method stub
		ClienteDto clienteDto = clienteMapper.informacionClienteDtoToClienteDto(informacion);
		ResponseEntity<TipoIdentificacionDto> identificacionDto = clienteService.verDetalleIdentificacion(informacion.getIdTipoIdentificacion());
		if(null!=identificacionDto) {
			clienteDto.setTipoIdentificacion(identificacionDto.getBody());
		}
		ResponseEntity<ClienteDto> clientReturn = clienteService.actualizarCliente(clienteDto);
		ClienteFotoDto fotoDto = ClienteFotoDto.builder()
				.id(informacion.getId())
				.img(informacion.getImg())
				.build();
		ResponseEntity<ClienteFotoDto> fotoReturn = fotoService.actualizarFoto(fotoDto);
		InformacionClienteDto infoReturn = clienteMapper.clientDtoToInformacionClienteDto(clientReturn!=null? clientReturn.getBody():null);
		if (null!=infoReturn && null!=fotoReturn && null!=fotoReturn.getBody()){
			infoReturn.setImg(fotoReturn.getBody().getImg());
			infoReturn.setIdTipoIdentificacion(identificacionDto!=null && identificacionDto.getBody()!=null?identificacionDto.getBody().getId():null);
		}
		return infoReturn;
	}

	@Override
	public InformacionClienteDto mostrar(Long idCliente) {
		// TODO Auto-generated method stub
		ResponseEntity<ClienteDto> clienteReturn = clienteService.verDetalleCliente(idCliente);
		ResponseEntity<ClienteFotoDto> fotoReturn = fotoService.buscarFotoPorIdCliente(idCliente);
		InformacionClienteDto informacion = clienteMapper.clientDtoToInformacionClienteDto(clienteReturn!=null?clienteReturn.getBody():null);
		if(null!=informacion) {
			if(null!=clienteReturn && null!=clienteReturn.getBody() && null!=clienteReturn.getBody().getTipoIdentificacion()) {
				informacion.setIdTipoIdentificacion(clienteReturn.getBody().getTipoIdentificacion().getId());
			}
			if(null!=fotoReturn && null!=fotoReturn.getBody()) {
				informacion.setImg(fotoReturn.getBody().getImg());	
			}
		}
		informacion.setId(idCliente);
		return informacion;
	}



}
