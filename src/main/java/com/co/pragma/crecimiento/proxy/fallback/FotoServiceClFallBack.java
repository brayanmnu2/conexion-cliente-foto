package com.co.pragma.crecimiento.proxy.fallback;

import javax.validation.Valid;

import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.co.pragma.crecimiento.dto.ClienteFotoDto;
import com.co.pragma.crecimiento.proxy.FotoServiceCl;

@Service
public class FotoServiceClFallBack implements FotoServiceCl{

	@Override
	public ResponseEntity<ClienteFotoDto> actualizarFoto(ClienteFotoDto clienteFotoDto) {
		ClienteFotoDto fotoReturn = ClienteFotoDto.builder()
				.id(null)
				.img("")
				.build();
		return ResponseEntity.ok(fotoReturn);
	}

	@Override
	public ResponseEntity<ClienteFotoDto> buscarFotoPorIdCliente(Long idCliente) {
		ClienteFotoDto fotoReturn = ClienteFotoDto.builder()
				.id(null)
				.img("")
				.build();
		return ResponseEntity.ok(fotoReturn);
	}

	@Override
	public ResponseEntity<ClienteFotoDto> registrarFoto(@Valid ClienteFotoDto clienteFotoDto) {
		ClienteFotoDto fotoReturn = ClienteFotoDto.builder()
				.id(null)
				.img("")
				.build();
		return ResponseEntity.ok(fotoReturn);
	}

}
