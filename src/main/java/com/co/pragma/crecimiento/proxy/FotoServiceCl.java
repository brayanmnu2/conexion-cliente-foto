package com.co.pragma.crecimiento.proxy;

import javax.validation.Valid;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;

import com.co.pragma.crecimiento.dto.ClienteFotoDto;
import com.co.pragma.crecimiento.proxy.fallback.FotoServiceClFallBack;


@FeignClient(name="cliente-foto-service", fallback = FotoServiceClFallBack.class)
public interface FotoServiceCl {
	
	@PostMapping(value="/cliente-foto/registrar")
	public ResponseEntity<ClienteFotoDto> registrarFoto(@Valid @RequestBody ClienteFotoDto clienteFotoDto/*, BindingResult result*/);

	@PutMapping(value="/cliente-foto/actualizar")
	public ResponseEntity<ClienteFotoDto> actualizarFoto(@Valid @RequestBody ClienteFotoDto clienteFotoDto);

	@GetMapping(value="/cliente-foto/buscarFotoPorIdCliente/{idCliente}")
	public ResponseEntity<ClienteFotoDto> buscarFotoPorIdCliente(@PathVariable("idCliente") Long idCliente);
}
