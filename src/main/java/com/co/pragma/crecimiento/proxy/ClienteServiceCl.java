package com.co.pragma.crecimiento.proxy;


import javax.validation.Valid;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;

import com.co.pragma.crecimiento.dto.ClienteDto;
import com.co.pragma.crecimiento.dto.TipoIdentificacionDto;
import com.co.pragma.crecimiento.proxy.fallback.ClienteServiceClFallback;

@FeignClient(value="cliente-service", fallback = ClienteServiceClFallback.class)
public interface ClienteServiceCl {

	@PostMapping(value="/cliente/registrar")
	public ResponseEntity<ClienteDto> registrarCliente(@Valid @RequestBody ClienteDto clienteDto/*, BindingResult result*/);
	
	@PutMapping(value="/cliente/actualizar")
	public ResponseEntity<ClienteDto> actualizarCliente(@Valid @RequestBody ClienteDto clienteDto);
	
	@GetMapping(value="/cliente/verDetalleCliente/{idCliente}")
	public ResponseEntity<ClienteDto> verDetalleCliente(@PathVariable("idCliente") Long idCliente);
	
	@GetMapping(value="/tipo-identificacion/verDetalle/{id}")
	public ResponseEntity<TipoIdentificacionDto> verDetalleIdentificacion(@PathVariable("id") Long id);
}
