package com.co.pragma.crecimiento.proxy.fallback;

import javax.validation.Valid;

import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.co.pragma.crecimiento.dto.ClienteDto;
import com.co.pragma.crecimiento.dto.TipoIdentificacionDto;
import com.co.pragma.crecimiento.proxy.ClienteServiceCl;

@Service
public class ClienteServiceClFallback implements ClienteServiceCl{

	@Override
	public ResponseEntity<ClienteDto> actualizarCliente(ClienteDto clienteDto) {
		ClienteDto clienteReturn = ClienteDto.builder()
				.apellidos("")
				.nombres("")
				.edad(0)
				.nroIdentificacion("")
				.ciudadNacimiento("")
				.build();
		return ResponseEntity.ok(clienteReturn);
	}

	@Override
	public ResponseEntity<ClienteDto> verDetalleCliente(Long idCliente) {
		// TODO Auto-generated method stub
		ClienteDto clienteReturn = ClienteDto.builder()
				.apellidos("")
				.nombres("")
				.edad(0)
				.nroIdentificacion("")
				.ciudadNacimiento("")
				.build();
		return ResponseEntity.ok(clienteReturn);
	}
	
	@Override
	public ResponseEntity<TipoIdentificacionDto> verDetalleIdentificacion(Long id) {
		TipoIdentificacionDto tipoDto = TipoIdentificacionDto.builder()
				.id(1L)
				.descripcion(null)
				.build();
		return ResponseEntity.ok(tipoDto);
	}

	@Override
	public ResponseEntity<ClienteDto> registrarCliente(@Valid ClienteDto clienteDto) {
		ClienteDto clienteReturn = ClienteDto.builder()
				.apellidos("")
				.nombres("")
				.edad(0)
				.nroIdentificacion("")
				.ciudadNacimiento("")
				.build();
		return ResponseEntity.ok(clienteReturn);
	}

}
