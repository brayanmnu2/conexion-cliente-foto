package com.co.pragma.crecimiento.dto;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class ClienteDto {

	private Long id;
	private String nombres;
	private String apellidos;
	private TipoIdentificacionDto tipoIdentificacion;
	private String nroIdentificacion;
	private int edad;
	private String ciudadNacimiento;

}
