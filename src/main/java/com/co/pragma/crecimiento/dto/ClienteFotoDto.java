package com.co.pragma.crecimiento.dto;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class ClienteFotoDto {
	private Long id;
	private String img;
}
