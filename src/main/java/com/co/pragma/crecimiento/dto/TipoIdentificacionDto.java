package com.co.pragma.crecimiento.dto;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class TipoIdentificacionDto {

	private Long id;
	private String descripcion;
}
