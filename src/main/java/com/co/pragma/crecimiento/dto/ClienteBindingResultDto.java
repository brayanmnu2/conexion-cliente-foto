package com.co.pragma.crecimiento.dto;

import javax.validation.Valid;

import org.springframework.validation.BindingResult;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Builder;

import lombok.Data;

@Data
@Builder
public class ClienteBindingResultDto {
	@Valid
	@JsonIgnoreProperties({ "hibernateLazyInitializer", "handler" })
	private ClienteDto clienteDto;
	private BindingResult result;
}
