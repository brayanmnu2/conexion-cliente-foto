package com.co.pragma.crecimiento.controller;

import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import com.co.pragma.crecimiento.dto.InformacionClienteDto;
import com.co.pragma.crecimiento.service.ConexionClienteFotoService;
import com.co.pragma.crecimiento.util.UtilPractica;


@RestController
@RequestMapping(value="conexion-cliente-foto")
public class ConexionClienteFotoController {

	@Autowired
	private ConexionClienteFotoService conexionService;
	
	
	@PostMapping(value="registrar-convert-base64")
	public ResponseEntity<InformacionClienteDto> registrarConvertBase64(@RequestPart("img") MultipartFile file ,
			@RequestPart("clienteFotoJson") @Valid InformacionClienteDto informacionDto){
		UtilPractica util = new UtilPractica();
		String imgBase64 = util.convertMultipartToBase64(file);
		informacionDto.setImg(imgBase64);
		InformacionClienteDto clienteReturn = conexionService.registrar(informacionDto);
		if(null==clienteReturn) {
			return ResponseEntity.badRequest().build();
		}
		return ResponseEntity.status(HttpStatus.CREATED).body(clienteReturn);
	}
	
	@PutMapping(value="actualizar-convert-base64")
	public ResponseEntity<InformacionClienteDto> actualizarConvertBase64(@RequestPart("img") MultipartFile file ,
			@RequestPart("clienteFotoJson") @Valid  InformacionClienteDto informacionDto){
		if(null!=informacionDto.getId()) {
			UtilPractica util = new UtilPractica();
			String imgBase64 = util.convertMultipartToBase64(file);
			informacionDto.setImg(imgBase64);
			InformacionClienteDto clienteReturn = conexionService.actualizar(informacionDto);
			if(null!=clienteReturn) {
				return ResponseEntity.ok(clienteReturn);
			}
		}
		return ResponseEntity.badRequest().build();
		
	}
	
	@PostMapping(value="registrar")
	public ResponseEntity<InformacionClienteDto> registrar(@Valid @RequestBody InformacionClienteDto informacionDto/*, BindingResult result*/){
		
		InformacionClienteDto clienteReturn = conexionService.registrar(informacionDto);
		if(null==clienteReturn) {
			return ResponseEntity.badRequest().build();
		}
		return ResponseEntity.status(HttpStatus.CREATED).body(clienteReturn);
	}
	
	
	@PutMapping(value="actualizar")
	public ResponseEntity<InformacionClienteDto> actualizar(@Valid @RequestBody InformacionClienteDto informacionDto){
		if(null!=informacionDto.getId()) {
			InformacionClienteDto clienteReturn = conexionService.actualizar(informacionDto);
			if(null!=clienteReturn) {
				return ResponseEntity.ok(clienteReturn);
			}
		}
		return ResponseEntity.badRequest().build();
	}
	
	@GetMapping(value="mostrar/{idCliente}")
	public ResponseEntity<InformacionClienteDto> mostrar(@PathVariable("idCliente") Long idCliente){
		InformacionClienteDto informacion = conexionService.mostrar(idCliente);
		if(null==informacion) {
			return ResponseEntity.noContent().build();
		}
		return ResponseEntity.ok(informacion);
	}
}
