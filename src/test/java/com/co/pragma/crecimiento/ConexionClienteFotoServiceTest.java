package com.co.pragma.crecimiento;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.Mockito.*;
import org.junit.jupiter.api.Test;
import org.mapstruct.factory.Mappers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.springframework.boot.test.context.SpringBootTest;

import com.co.pragma.crecimiento.dto.InformacionClienteDto;
import com.co.pragma.crecimiento.mapper.ClienteMapper;
import com.co.pragma.crecimiento.proxy.ClienteServiceCl;
import com.co.pragma.crecimiento.proxy.FotoServiceCl;
import com.co.pragma.crecimiento.service.impl.ConexionClienteFotoServiceImpl;
import com.co.pragma.crecimiento.util.DatosTest;

@SpringBootTest
public class ConexionClienteFotoServiceTest {

	@Mock
	ClienteServiceCl clienteService;
	
	@Mock
	FotoServiceCl fotoService;

	@Spy
	ClienteMapper clienteMapper = Mappers.getMapper(ClienteMapper.class);

	@InjectMocks
	ConexionClienteFotoServiceImpl conexionService;

	@Test
	void testRegistrarExitoso() {
		when(clienteService.verDetalleIdentificacion(DatosTest.UNO_LONG)).thenReturn(DatosTest.TIPO_IDENTIFICACION_DTO_RESPONSE_DNI_OK);
		when(clienteService.registrarCliente(DatosTest.CLIENTE_DTO_SIN_ID_001)).thenReturn(DatosTest.CLIENTE_DTO_RESPONSE_001);
		when(fotoService.registrarFoto(DatosTest.FOTO_DTO_001)).thenReturn(DatosTest.FOTO_DTO_RESPONSE_001);
		
		InformacionClienteDto informacionReturn = conexionService.registrar(DatosTest.INFORMACION_CLIENTE_DTO_001);
		
		assertNotNull(informacionReturn);
		assertEquals("Neyra Uriarte", informacionReturn.getApellidos());
		assertEquals("imagen 1", informacionReturn.getImg());
		verify(clienteService).verDetalleIdentificacion(anyLong());
		verify(clienteService).registrarCliente(any());
		verify(fotoService).registrarFoto(any());
	}
	

	@Test
	void testRegistrarClienteServiceFallback() {
		when(clienteService.verDetalleIdentificacion(anyLong())).thenReturn(DatosTest.TIPO_IDENTIFICACION_DTO_RESPONSE_DNI_FALLBACK);
		when(clienteService.registrarCliente(any())).thenReturn(DatosTest.CLIENTE_DTO_RESPONSE_FALLBACK_001);
		when(fotoService.registrarFoto(any())).thenReturn(DatosTest.FOTO_DTO_RESPONSE_FALLBACK_001);
		
		InformacionClienteDto informacionReturn = conexionService.registrar(DatosTest.INFORMACION_CLIENTE_DTO_001);
		
		assertNotNull(informacionReturn);
		assertEquals("", informacionReturn.getApellidos());
		verify(clienteService).verDetalleIdentificacion(anyLong());
		verify(clienteService).registrarCliente(any());
		verify(fotoService).registrarFoto(any());
	}
	
	@Test
	void testActualizarExitoso() {
		when(clienteService.verDetalleIdentificacion(DatosTest.UNO_LONG)).thenReturn(DatosTest.TIPO_IDENTIFICACION_DTO_RESPONSE_DNI_OK);
		when(clienteService.actualizarCliente(DatosTest.CLIENTE_DTO_MODIFICADO_001)).thenReturn(DatosTest.CLIENTE_DTO_RESPONSE_MODIFICADO_001);
		when(fotoService.actualizarFoto(DatosTest.FOTO_DTO_MODIFICADO_001)).thenReturn(DatosTest.FOTO_DTO_MODIFICADO_RESPONSE_001);
		

		InformacionClienteDto informacionReturn = conexionService.actualizar(DatosTest.INFORMACION_CLIENTE_DTO_MODIFICADO_001);
		
		assertNotNull(informacionReturn);
		assertEquals("Brayitan Michel", informacionReturn.getNombres());
		assertEquals("imagen 12345", informacionReturn.getImg());
		verify(clienteService).verDetalleIdentificacion(anyLong());
		verify(clienteService).actualizarCliente(any());
		verify(fotoService).actualizarFoto(any());
	}
	
	@Test
	void testActualizarFallback() {
		when(clienteService.verDetalleIdentificacion(anyLong())).thenReturn(DatosTest.TIPO_IDENTIFICACION_DTO_RESPONSE_DNI_FALLBACK);
		when(clienteService.actualizarCliente(any())).thenReturn(DatosTest.CLIENTE_DTO_RESPONSE_FALLBACK_001);
		when(fotoService.actualizarFoto(any())).thenReturn(DatosTest.FOTO_DTO_RESPONSE_FALLBACK_001);
		

		InformacionClienteDto informacionReturn = conexionService.actualizar(DatosTest.INFORMACION_CLIENTE_DTO_MODIFICADO_001);
		
		assertNotNull(informacionReturn);
		assertEquals("", informacionReturn.getNombres());
		assertEquals("", informacionReturn.getImg());
		verify(clienteService).verDetalleIdentificacion(anyLong());
		verify(clienteService).actualizarCliente(any());
		verify(fotoService).actualizarFoto(any());
	}
	
	@Test
	void testMostrarExitoso() {
		when(clienteService.verDetalleCliente(DatosTest.UNO_LONG)).thenReturn(DatosTest.CLIENTE_DTO_RESPONSE_001);
		when(fotoService.buscarFotoPorIdCliente(DatosTest.UNO_LONG)).thenReturn(DatosTest.FOTO_DTO_RESPONSE_001);

		InformacionClienteDto informacionReturn = conexionService.mostrar(DatosTest.UNO_LONG);
		
		assertNotNull(informacionReturn);
		assertEquals("Neyra Uriarte", informacionReturn.getApellidos());
		assertEquals("imagen 1", informacionReturn.getImg());
		verify(clienteService).verDetalleCliente(anyLong());
		verify(fotoService).buscarFotoPorIdCliente(anyLong());
		
	}
	
	@Test
	void testMostrarFallback() {
		when(clienteService.verDetalleCliente(DatosTest.UNO_LONG)).thenReturn(DatosTest.CLIENTE_DTO_RESPONSE_FALLBACK_001);
		when(fotoService.buscarFotoPorIdCliente(DatosTest.UNO_LONG)).thenReturn(DatosTest.FOTO_DTO_RESPONSE_FALLBACK_001);

		InformacionClienteDto informacionReturn = conexionService.mostrar(DatosTest.UNO_LONG);
		
		assertNotNull(informacionReturn);
		assertEquals("", informacionReturn.getApellidos());
		assertEquals("", informacionReturn.getImg());
		verify(clienteService).verDetalleCliente(anyLong());
		verify(fotoService).buscarFotoPorIdCliente(anyLong());
		
	}
	
}
