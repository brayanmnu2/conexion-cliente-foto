package com.co.pragma.crecimiento;

import static org.hamcrest.Matchers.hasSize;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mapstruct.factory.Mappers;
import org.mockito.Spy;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMultipartHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.request.RequestPostProcessor;

import com.co.pragma.crecimiento.controller.ConexionClienteFotoController;
import com.co.pragma.crecimiento.dto.InformacionClienteDto;
import com.co.pragma.crecimiento.mapper.ClienteMapper;
import com.co.pragma.crecimiento.service.ConexionClienteFotoService;
import com.co.pragma.crecimiento.util.DatosTest;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

@WebMvcTest(ConexionClienteFotoController.class)
public class ConexionClienteFotoControllerTest {
	ObjectMapper objectMapper;

	@Autowired
	private MockMvc mvc;

	@MockBean
	private ConexionClienteFotoService conexionService;

	@Spy
	ClienteMapper clienteMapper = Mappers.getMapper(ClienteMapper.class);

	@BeforeEach
	void setUp() {
		objectMapper = new ObjectMapper();
	}

	@Test
	void testRegistrarConvertBase64Exitoso() throws Exception {

		when(conexionService.registrar(any())).then(invocation -> {
			InformacionClienteDto i = invocation.getArgument(0);
			i.setId(1L);
			return i;
		});

		MockMultipartFile img = new MockMultipartFile("img", "imagen.png", "image/png",
				"Imagen para convertir a base64".getBytes());
		MockMultipartFile clienteFotoJson = new MockMultipartFile("clienteFotoJson", "clienteFotoJson.json",
				"application/json",
				"{\"id\": \"\",\r\n \"nombres\": \"Paolo\",\r\n\"apellidos\": \"Guerrero\",\r\n\"idTipoIdentificacion\": 1,\r\n \"nroIdentificacion\": \"68575339\",\r\n\"edad\": 40,\r\n\"ciudadNacimiento\": \"Lima\",\r\n\"img\": \"\"\r\n}"
						.getBytes());

		mvc.perform(MockMvcRequestBuilders.multipart("/conexion-cliente-foto/registrar-convert-base64").file(img).file(clienteFotoJson))
				.andExpect(status().isCreated())
				.andExpect(content().contentType(MediaType.APPLICATION_JSON))
				.andExpect(jsonPath("$.nombres").value("Paolo"));

		verify(conexionService).registrar(any());
	}
	
	@Test
	void testRegistrarConvertBase64NoExitoso() throws Exception {

		when(conexionService.registrar(any())).thenReturn(null);

		MockMultipartFile img = new MockMultipartFile("img", "imagen.png", "image/png",
				"Imagen para convertir a base64".getBytes());
		MockMultipartFile clienteFotoJson = new MockMultipartFile("clienteFotoJson", "clienteFotoJson.json",
				"application/json",
				"{\"id\": \"\",\r\n \"nombres\": \"Paolo\",\r\n\"apellidos\": \"Guerrero\",\r\n\"idTipoIdentificacion\": 1,\r\n \"nroIdentificacion\": \"68575339\",\r\n\"edad\": 40,\r\n\"ciudadNacimiento\": \"Lima\",\r\n\"img\": \"\"\r\n}"
						.getBytes());

		mvc.perform(MockMvcRequestBuilders.multipart("/conexion-cliente-foto/registrar-convert-base64").file(img).file(clienteFotoJson))
				.andExpect(status().isBadRequest());

		verify(conexionService).registrar(any());
	}
	
	@Test
	void testRegistrarConvertBase64NoValid() throws Exception {

		when(conexionService.registrar(any())).then(invocation -> {
			InformacionClienteDto i = invocation.getArgument(0);
			i.setId(1L);
			return i;
		});

		MockMultipartFile img = new MockMultipartFile("img", "imagen.png", "image/png",
				"Imagen para convertir a base64".getBytes());
		MockMultipartFile clienteFotoJson = new MockMultipartFile("clienteFotoJson", "clienteFotoJson.json",
				"application/json",
				"{\"id\": \"\",\r\n \"nombres\": \"\",\r\n\"apellidos\": \"Guerrero\",\r\n\"idTipoIdentificacion\": 1,\r\n \"nroIdentificacion\": \"68575339\",\r\n\"edad\": 40,\r\n\"ciudadNacimiento\": \"Lima\",\r\n\"img\": \"\"\r\n}"
						.getBytes());

		mvc.perform(MockMvcRequestBuilders.multipart("/conexion-cliente-foto/registrar-convert-base64").file(img).file(clienteFotoJson))
				.andExpect(status().isBadRequest());

	}
	
	@Test
	void testActualizarConvertBase64Exitoso() throws Exception {

		when(conexionService.actualizar(any())).then(invocation -> {
			InformacionClienteDto i = invocation.getArgument(0);
			return i;
		});

		MockMultipartFile img = new MockMultipartFile("img", "imagen.png", "image/png",
				"Imagen para convertir a base64".getBytes());
		MockMultipartFile clienteFotoJson = new MockMultipartFile("clienteFotoJson", "clienteFotoJson.json",
				"application/json",
				"{\"id\": \"1\",\r\n \"nombres\": \"Paolo123\",\r\n\"apellidos\": \"Guerrero\",\r\n\"idTipoIdentificacion\": 1,\r\n \"nroIdentificacion\": \"68575339\",\r\n\"edad\": 40,\r\n\"ciudadNacimiento\": \"Lima\",\r\n\"img\": \"\"\r\n}"
						.getBytes());


	    MockMultipartHttpServletRequestBuilder builder =
	            MockMvcRequestBuilders.multipart("/conexion-cliente-foto/actualizar-convert-base64");
	    builder.with(new RequestPostProcessor() {
	        @Override
	        public MockHttpServletRequest postProcessRequest(MockHttpServletRequest request) {
	            request.setMethod("PUT");
	            return request;
	        }
	    });
	    
		mvc.perform(builder.file(img).file(clienteFotoJson))
				.andExpect(status().isOk())
				.andExpect(content().contentType(MediaType.APPLICATION_JSON))
				.andExpect(jsonPath("$.nombres").value("Paolo123"));

		verify(conexionService).actualizar(any());
	}
	
	@Test
	void testActualizarConvertBase64NoExitoso() throws Exception {

		when(conexionService.actualizar(any())).thenReturn(null);

		MockMultipartFile img = new MockMultipartFile("img", "imagen.png", "image/png",
				"Imagen para convertir a base64".getBytes());
		MockMultipartFile clienteFotoJson = new MockMultipartFile("clienteFotoJson", "clienteFotoJson.json",
				"application/json",
				"{\"id\": \"1\",\r\n \"nombres\": \"Paolo123\",\r\n\"apellidos\": \"Guerrero\",\r\n\"idTipoIdentificacion\": 1,\r\n \"nroIdentificacion\": \"68575339\",\r\n\"edad\": 40,\r\n\"ciudadNacimiento\": \"Lima\",\r\n\"img\": \"\"\r\n}"
						.getBytes());


	    MockMultipartHttpServletRequestBuilder builder =
	            MockMvcRequestBuilders.multipart("/conexion-cliente-foto/actualizar-convert-base64");
	    builder.with(new RequestPostProcessor() {
	        @Override
	        public MockHttpServletRequest postProcessRequest(MockHttpServletRequest request) {
	            request.setMethod("PUT");
	            return request;
	        }
	    });
		mvc.perform(builder.file(img).file(clienteFotoJson))
				.andExpect(status().isBadRequest());

		verify(conexionService).actualizar(any());
	}

	@Test
	void testActualizarConvertBase64NoExitosoNoId() throws Exception {

		when(conexionService.actualizar(any())).then(invocation -> {
			InformacionClienteDto i = invocation.getArgument(0);
			return i;
		});

		MockMultipartFile img = new MockMultipartFile("img", "imagen.png", "image/png",
				"Imagen para convertir a base64".getBytes());
		MockMultipartFile clienteFotoJson = new MockMultipartFile("clienteFotoJson", "clienteFotoJson.json",
				"application/json",
				"{\"id\": \"\",\r\n \"nombres\": \"Paolo123\",\r\n\"apellidos\": \"Guerrero\",\r\n\"idTipoIdentificacion\": 1,\r\n \"nroIdentificacion\": \"68575339\",\r\n\"edad\": 40,\r\n\"ciudadNacimiento\": \"Lima\",\r\n\"img\": \"\"\r\n}"
						.getBytes());


	    MockMultipartHttpServletRequestBuilder builder =
	            MockMvcRequestBuilders.multipart("/conexion-cliente-foto/actualizar-convert-base64");
	    builder.with(new RequestPostProcessor() {
	        @Override
	        public MockHttpServletRequest postProcessRequest(MockHttpServletRequest request) {
	            request.setMethod("PUT");
	            return request;
	        }
	    });
	    
		mvc.perform(builder.file(img).file(clienteFotoJson))
				.andExpect(status().isBadRequest());

	}
	

	@Test
	void testActualizarConvertBase64NoValid() throws Exception {

		when(conexionService.actualizar(any())).then(invocation -> {
			InformacionClienteDto i = invocation.getArgument(0);
			return i;
		});

		MockMultipartFile img = new MockMultipartFile("img", "imagen.png", "image/png",
				"Imagen para convertir a base64".getBytes());
		MockMultipartFile clienteFotoJson = new MockMultipartFile("clienteFotoJson", "clienteFotoJson.json",
				"application/json",
				"{\"id\": \"1\",\r\n \"nombres\": \"\",\r\n\"apellidos\": \"Guerrero\",\r\n\"idTipoIdentificacion\": 1,\r\n \"nroIdentificacion\": \"68575339\",\r\n\"edad\": 40,\r\n\"ciudadNacimiento\": \"Lima\",\r\n\"img\": \"\"\r\n}"
						.getBytes());


	    MockMultipartHttpServletRequestBuilder builder =
	            MockMvcRequestBuilders.multipart("/conexion-cliente-foto/actualizar-convert-base64");
	    builder.with(new RequestPostProcessor() {
	        @Override
	        public MockHttpServletRequest postProcessRequest(MockHttpServletRequest request) {
	            request.setMethod("PUT");
	            return request;
	        }
	    });
	    
		mvc.perform(builder.file(img).file(clienteFotoJson))
				.andExpect(status().isBadRequest());

	}
	
	@Test
	void testRegistrarExitoso() throws JsonProcessingException, Exception {
		when(conexionService.registrar(any())).then(invocation -> {
			InformacionClienteDto i = invocation.getArgument(0);
			i.setId(1L);
			return i;
		});
		
		
		mvc.perform(post("/conexion-cliente-foto/registrar").contentType(MediaType.APPLICATION_JSON)
				.content(objectMapper.writeValueAsString(DatosTest.INFORMACION_CLIENTE_DTO_001)))
		.andExpect(status().isCreated())
		.andExpect(content().contentType(MediaType.APPLICATION_JSON))
		.andExpect(content().json(objectMapper.writeValueAsString(DatosTest.INFORMACION_CLIENTE_RETURN_DTO_001)));
		
		verify(conexionService).registrar(any());
	}
	
	@Test
	void testRegistrarNoExitoso() throws JsonProcessingException, Exception {
		when(conexionService.registrar(any())).thenReturn(null);
		
		
		mvc.perform(post("/conexion-cliente-foto/registrar").contentType(MediaType.APPLICATION_JSON)
				.content(objectMapper.writeValueAsString(DatosTest.INFORMACION_CLIENTE_DTO_001)))
		.andExpect(status().isBadRequest());
		
		verify(conexionService).registrar(any());
	}
	

	@Test
	void testRegistrarNoValid() throws JsonProcessingException, Exception {
		when(conexionService.registrar(any())).thenReturn(null);
		
		
		mvc.perform(post("/conexion-cliente-foto/registrar").contentType(MediaType.APPLICATION_JSON)
				.content(objectMapper.writeValueAsString(DatosTest.INFORMACION_CLIENTE_NO_VALID_DTO_001)))
		.andExpect(status().isBadRequest());
		
	}
	
	@Test
	void testActualizarExitoso() throws JsonProcessingException, Exception {
		when(conexionService.actualizar(any())).then(invocation -> {
			InformacionClienteDto i = invocation.getArgument(0);
			return i;
		});
		

		mvc.perform(put("/conexion-cliente-foto/actualizar").contentType(MediaType.APPLICATION_JSON)
				.content(objectMapper.writeValueAsString(DatosTest.INFORMACION_CLIENTE_DTO_MODIFICADO_001)))
		.andExpect(status().isOk())
		.andExpect(content().contentType(MediaType.APPLICATION_JSON))
		.andExpect(content().json(objectMapper.writeValueAsString(DatosTest.INFORMACION_CLIENTE_DTO_MODIFICADO_001)));
		
		verify(conexionService).actualizar(any());
	}
	
	
	@Test
	void testActualizarNoExitoso() throws JsonProcessingException, Exception {
		when(conexionService.actualizar(any())).thenReturn(null);
		
		mvc.perform(put("/conexion-cliente-foto/actualizar").contentType(MediaType.APPLICATION_JSON)
				.content(objectMapper.writeValueAsString(DatosTest.INFORMACION_CLIENTE_DTO_MODIFICADO_001)))
		.andExpect(status().isBadRequest());
		
		verify(conexionService).actualizar(any());
	}
	
	
	@Test
	void testActualizarNoValid() throws JsonProcessingException, Exception {
		when(conexionService.actualizar(any())).then(invocation -> {
			InformacionClienteDto i = invocation.getArgument(0);
			return i;
		});
		

		mvc.perform(put("/conexion-cliente-foto/actualizar").contentType(MediaType.APPLICATION_JSON)
				.content(objectMapper.writeValueAsString(DatosTest.INFORMACION_CLIENTE_NO_VALID_DTO_001)))
		.andExpect(status().isBadRequest());
		
	}
	
	@Test
	void testMostrarExitoso() throws JsonProcessingException, Exception {
		when(conexionService.mostrar(DatosTest.UNO_LONG)).thenReturn(DatosTest.INFORMACION_CLIENTE_RETURN_DTO_001);
		

		mvc.perform(get("/conexion-cliente-foto/mostrar/1").contentType(MediaType.APPLICATION_JSON))
		.andExpect(status().isOk())
		.andExpect(content().contentType(MediaType.APPLICATION_JSON))
		.andExpect(jsonPath("$.nroIdentificacion").value("75153630"))
		.andExpect(jsonPath("$.img").value("imagen 1"))
		//comparacion mas robusta
		.andExpect(content().json(objectMapper.writeValueAsString(DatosTest.INFORMACION_CLIENTE_RETURN_DTO_001)));
		
		verify(conexionService).mostrar(anyLong());
	}
	
	@Test
	void testMostrarNoExitoso() throws JsonProcessingException, Exception {
		when(conexionService.mostrar(DatosTest.UNO_LONG)).thenReturn(null);
		

		mvc.perform(get("/conexion-cliente-foto/mostrar/1").contentType(MediaType.APPLICATION_JSON))
		.andExpect(status().isNoContent());
		
		verify(conexionService).mostrar(anyLong());
	}

}
